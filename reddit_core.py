import argparse
import json
import os
import sys

import requests
from PIL import Image

verbose = False
comments_depth = 0

headers = {
    'Referer':
    'http://www.reddit.com/',
    'User-Agent':
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
}


def download_more(subreddit, after=None):
    params = {
        'sort': 'new',
        'allow_over18': '1'
    }

    if after:
        params['after'] = after

    r = requests.get(
        f'https://gateway.reddit.com/desktopapi/v1/subreddits/{subreddit}',
        params=params,
        headers=headers
    )

    if r.status_code != 200:
        raise ConnectionError

    content = json.loads(r.content)
    return (
        [content['posts'][postId] for postId in content['postIds']],
        content['postIds'][-1],
    )


def download_id(postId):
    if postId in (None, ''):
        return -1
    if not postId.startswith('t3_'):
        return 'Miss t3_'

    params = {}

    if comments_depth:
        params['depth'] = comments_depth

    r = requests.get(
        f'https://gateway.reddit.com/desktopapi/v1/postcomments/{postId}',
        headers=headers,
        params=params
    )

    if r.status_code != 200:
        raise ConnectionError

    content = json.loads(r.content)

    subredditId = list(content['subreddits'].keys())[0]
    subreddit = content['subreddits'][subredditId]['name']
    post = content['posts'][postId]
    comments = content['comments']

    if verbose:
        print(post)

    post_data = parse_post(post, subreddit, comments)

    yield post_data


def download_file(link):

    ext = link.split('.')[-1].split('?')[0]

    r = requests.get(
        link,
        headers=headers,
        stream=True,
    )

    if r.status_code != 200:
        return -1

    return r, ext


def parse_embed(post, postId):

    post_data = None
    data = None

    if post['domain'] == 'imgur.com':
        data = download_file(post['source']['url'] + '.png')
    elif post['domain'] == 'i.redd.it':
        data = download_file(post['media']['content'])
    elif post['domain'] == 'v.redd.it':
        data = post['media']['dashUrl'].split('?')[0], 'mp4'
    elif verbose:
        print(f'[embed] post: {json.dumps(post)}')

    if data:
        raw, ext = data

        post_data = {
            'title': post['title'],
            'id': postId,
            'data': raw,
            'ext': ext
        }

        if post['media']['type'] in ('video'):
            post_data['media'] = 'video'
        elif post['media']['type'] in ('image'):
            post_data['media'] = 'image'
        elif post['media']['type'] in ('gifvideo'):
            post_data['media'] = 'gif'
        else:
            post_data['media'] = None

    return post_data

def download_next(post):
    _next = download_more(post['subreddit'], post['id'])[0][0]
    next_post = parse_post(_next, post['subreddit'])
    yield next_post

def comments_tree(comments, subreddit):
    '''
    comments_list[comment] = {
        'author' : data['author'],
        'authorId' : data['authorId'],
        'bodyMD' : data['bodyMD'],
        'next' : {}
    }
    '''
    print(f'[comment] {comments}')
    comments_list = {}

    print('Comments tree work in progress')


def parse_post(post, subreddit, comments=None):
    if post['media'] is None:
        return None

    postId = post['id']

    if verbose:
        print(f'[parse_post] post id: {postId}')

    post_data = None

    if post['media']['type'] == 'text':
        post_data = {
            'subreddit': subreddit,
            'media': 'text',
            'title': post['title'],
            'id': postId,
            'ext': 'md'
        }

        if 'markdownContent' in post['media']:
            post_data['data'] = post['media']['markdownContent']

    elif post['media']['type'] == 'image':
        if post['domain'] in ('i.redd.it', 'i.imgur.com'):
            raw, ext = download_file(post['media']['content'])

            post_data = {
                'subreddit': subreddit,
                'media': 'image',
                'title': post['title'],
                'id': postId,
                'data': raw,
                'ext': ext
            }

        elif verbose:
            print(f'[parse_post] domain: {post["domain"]}')

    elif post['media']['type'] in ('embed', 'gifvideo', 'video'):
        post_data = parse_embed(post, postId)
        if post_data:
            post_data['subreddit'] = subreddit

        if verbose:
            print(f'[embed] post data: {post_data}')

    if comments_depth:
        if comments is None:
            _, _, comments = next(download_id(postId))
        comments_tree(comments, subreddit)

    return post_data


def download_link(postLink):
    postId = 't3_' + postLink.split('comments/', 1)[1].split('/')[0]
    data = download_id(postId)
    return data


def download(subreddit, quantity=1):
    if quantity < 1 or subreddit in ('', None):
        return -1

    posts, after, count_post, i = [], None, 0, 0

    while count_post < quantity:
        if len(posts) == i:
            p, after = download_more(subreddit, after)
            posts.extend(p)
        post_data = parse_post(posts[i], subreddit)
        if post_data is not None:
            yield post_data
            count_post += 1
        i += 1


def parse_all(data_list):
    for data in data_list:
        if data.startswith('t3_'):
            post = download_id(data)
        elif data.startswith('https'):
            post = download_link(data)
        yield post


def folder_ids(subreddit, path):
    if not subreddit.lower() in map(lambda x: x.lower(), os.listdir(path)):
        os.mkdir(os.path.join(path, subreddit))

    ids = [
        _id.name.split('.')[0]
        for _id in os.scandir(os.path.join(path, subreddit))
        if _id.is_file()
    ]

    return ids


def download_data(data, path):

    if data is None:
        if verbose:
            print(f'[download] data is None')
        return

    for post in data:
        if post is None:
            continue

        subreddit = post['subreddit']
        ids = folder_ids(subreddit, path)
        full_path = os.path.join(path, subreddit)

        if verbose:
            print(f'[download] Post : {post}')

        if post['id'] in ids:
            if verbose:
                postId = post['id']
                print(f'[download] Already downloaded: {postId}')
        else:
            if post['media'] == 'text':
                with open(
                        os.path.join(full_path, post['id'] + '.' + post['ext']),
                        'w', encoding = 'utf-8') as f:
                    f.write(post['title'] + '\n\n')
                    if 'data' in post:
                        f.write(post['data'])

            elif post['media'] == 'image':
                img = Image.open(post['data'].raw)
                img.save(
                    os.path.join(full_path, post['id'] + '.' + post['ext'])
                )

            elif post['media'] == 'gif':
                with open(
                    os.path.join(full_path, post['id'] + '.' + post['ext']),
                    'wb'
                ) as f:
                    for chunk in post['data']:
                        f.write(chunk)

            elif post['media'] == 'video':
                try:
                    import youtube_dl

                    opts = {
                        'format'  : 'bestvideo+bestaudio/bestvideo',
                        'outtmpl' : full_path + '/' + post['id'] + '.mp4'
                    }

                    with youtube_dl.YoutubeDL(opts) as ydl:
                        info_dict = ydl.extract_info(post['data'], download=False)
                        ydl.download([post['data']], shell = False)

                except Exception as e:
                    os.system(
                        f'youtube-dl \
                        -f bestvideo+bestaudio/bestvideo \
                        -o {full_path}/{post["id"]}.mp4 \
                        {post["data"]}'
                    )

            elif verbose:
                print('[download] WIP')
        if get_next and input('Download next? [Y/n]').lower() in ['yes', '']:
            data = download_next(post)
            download_data(data, path)


def sys_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-q', '--quantity')
    parser.add_argument('-i', '--id')
    parser.add_argument('-l', '--link')
    parser.add_argument('-c', '--comments_depth')
    parser.add_argument('-p', '--path')
    parser.add_argument('-a', '--all', nargs = '+')
    parser.add_argument('-n', '--next', action='store_true')
    parser.add_argument('subreddit', nargs = '?')

    args = vars(parser.parse_args())

    if len(sys.argv) == 1:
        parser.error('Needs at least 1 argument')
        parser.print_help()
        return

    if args['quantity'] and args["subreddit"] is None:
        parser.error('subreddit name is required for --quantity')
    if args['comments_depth'] and (
        args["subreddit"] is None and \
        args["link"] is None and \
        args["id"] is None
    ):
        parser.error('post is required for --comments')

    return args



def main():
    global verbose
    global comments_depth
    global get_next

    args = sys_args()
    subreddit = args['subreddit']
    verbose = args['verbose']
    comments_depth = args['comments_depth']
    get_next = args['next']
    path = args['path']

    if subreddit is not None and subreddit.startswith('r/'):
        subreddit = subreddit[2:]
    if path is None:
        path = '.'

    if args['id']:
        data = download_id(args['id'])
    elif args['link']:
        data = download_link(args['link'])
    elif args['quantity']:
        data = download(subreddit, int(args['quantity']))
    elif args['all']:
        data = parse_all(args['all'])
    else:
        data = download(subreddit, 1)

    download_data(data, path)


if __name__ == '__main__':
    main()
