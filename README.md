# Reddit Downloader

A simple program to download text, images and videos from reddit.
----
Usage: `python3 reddit_core.py [params] (subreddit name)`
> download 1 post from (subreddit name)

Args:
- `-v (verbose)` show warning
- `-q (quantity)` download certain number of posts
- `-i (id)` download post from id [_t3_xxxxx_]
- `-l (link)` download post from link
- `-p (path)` path to main folder
- `-c (comments depth)` download comments [_WIP_]

## WIP
* [ ] Better code style
* [ ] Better & Custom params
* [ ] Download videos and gifs (and don't break images)
    * [x] Download video+audio
    * [x] Download only video
    * [x] Fixed gifs
    * [ ] Download video from other sources
* [ ] Finish comments download
* [ ] Probably handle errors better
* [ ] ...
----

Every function return a generator

The generator contains a dict with:
 - type ['image', 'text', ...]
 - extension
 - id
 - title
 - data
 - subreddit

or None if post does not exist, is an ad, or it is work in progress.

data should be a string of text or a `requests` object.
